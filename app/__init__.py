import os
import json

from flask import Flask

def create_app(test_config=None):
    # create and configure the app
    app = Flask(__name__, instance_relative_config=True)
    
    filename = "metadb.json"
    with open(filename, 'r') as f:
        datastore = json.load(f)
    
    POSTGRES = {
        'user': datastore["user"],
        'pw': datastore["password"],
        'db': datastore["dbname"],
        'host': datastore["host"],
        'port': datastore["port"],
    }
    
    app.config.from_mapping(
        SECRET_KEY='dev',
        DATABASE='postgresql://%(user)s:%(pw)s@%(host)s:%(port)s/%(db)s' % POSTGRES,
    )

    if test_config is None:
        # load the instance config, if it exists, when not testing
        app.config.from_pyfile('config.py', silent=True)
    else:
        # load the test config if passed in
        app.config.from_mapping(test_config)

    # ensure the instance folder exists
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    from . import db
    db.init_app(app)

    from . import auth
    app.register_blueprint(auth.bp)
    
    from . import blog
    app.register_blueprint(blog.bp)
    app.add_url_rule('/', endpoint='index')

    return app
    

# Implement a simple microblogging website like twitter.
# Features: user can log in or log out.
# Once logged in, everyone can see a global list of all messages (but only the last 20 messages).
# Users can post a message. The message list should show the user who posted the message.
# - Implement this locally using a Flask server
# - Use local instance of Postgres as your DB
# - Do NOT use an ORM like SQLAlchemy! Use vanilla SQL queries like a real masochist!
# - Use Firebase auth for authentication
# - Make your code look clean and concise!
# - Put all your work in a separate local git repository.


# Do this in parallel with your Python/Postgres learning.
# This will take some time to complete, not sure how much but at the earliest until end of week.
# Ask Ozair and Wasae for any backend related questions.
# Up to you how you want to do this: maybe read python and Postgres tutorials first,
# or start coding a little, get stuck, read a bit, then code again... whatever is best learning experience for you


# export VARIABLE_NAME=string